package ee.sda.testingbasics;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.AssertionsKt;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    // Annotations
    // Test method
    @Test
    public void AddAndMultiplyByThreeTest(){

        Calculator calculator = new Calculator();

        int actualResult = calculator.AddAndMultiplyByThree(2,2);

        Assertions.assertEquals(12, actualResult);
    }

}
